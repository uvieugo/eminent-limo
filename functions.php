<?php
/**
 * Eminent Limo functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Eminent_Limo
 */

if ( ! function_exists( 'eminent_limo_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function eminent_limo_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Eminent Limo, use a find and replace
		 * to change 'eminent-limo' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'eminent-limo', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'eminent-limo' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'eminent_limo_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		add_theme_support( 'post-formats', array( 'aside', 'gallery', 'image' ) );
	}
endif;
add_action( 'after_setup_theme', 'eminent_limo_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function eminent_limo_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'eminent_limo_content_width', 640 );
}
add_action( 'after_setup_theme', 'eminent_limo_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function eminent_limo_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'eminent-limo' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'eminent-limo' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'eminent_limo_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function eminent_limo_scripts() {
	wp_enqueue_style( 'eminent-limo-style', get_stylesheet_uri() );

	wp_enqueue_style( 'eminent-limo-style-2', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
	
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css' );

	wp_enqueue_script( 'eminent-limo-popper', get_template_directory_uri() . '/js/popper.min.js', array('jquery'), '20151215', true );
	
	wp_enqueue_script( 'eminent-limo-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '20151215', true );

	wp_enqueue_script( 'eminent-limo-custom', get_template_directory_uri() . '/js/eminent.js', array('jquery'), '20151215', true );

	wp_enqueue_script( 'eminent-limo-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'eminent-limo-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'eminent_limo_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// Register Custom Navigation Walker
require_once('bs4navwalker.php');

// remove_filter( 'the_content', 'wpautop' );
// remove_filter( 'the_excerpt', 'wpautop' );

function modify_read_more_link() {
    return '<a class="btn btn-custom more-link" href="' . get_permalink() . '">Read More</a>';
}
add_filter( 'the_content_more_link', 'modify_read_more_link' );

// function new_excerpt_more( $more ) {
// 	return '<a class="btn btn-custom more-link" href="' . get_permalink() . '">Read More</a>' ;
// }
// add_filter( 'excerpt_more', 'new_excerpt_more' );

function custom_excerpt( $excerpt ){
	$output .= '<p>' . $excerpt . '</p>';
	$output .= '<p><a class="btn btn-custom more-link" href="' . get_permalink() . '">Read More</a></p>';
	return $output;
}
add_filter( 'get_the_excerpt', 'custom_excerpt' );

// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );

function my_mce_before_init_insert_formats( $init_array ) {  
	// $init_array['theme_advanced_blockformats'] = 'Paragraph=p;Heading 3=h3;Heading 4=h4';
	$style_formats = array(  
			array(  
			'title' => 'post-h3',  
			'block' => 'h3',  
			'classes' => 'post-h3',
			'wrapper' => false,
		),
			array(  
			'title' => 'Intro',  
			'block' => 'p',  
			'classes' => 'teaser',
			'wrapper' => false,
			
		),  
			array(  
			'title' => 'book-now',  
			'selector' => 'a',  
			'classes' => 'btn btn-custom book-now',
			'wrapper' => false,
			
		), 
	);  
	$init_array['style_formats'] = json_encode( $style_formats );
	$init['style_formats_merge'] = false;
	return $init_array;  
} 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );  

function my_add_tinymce_button( $plugin_array ) {
	 $plugin_array['my_button_script'] = get_template_directory_uri() . '/js/tinymcs.js';
     return $plugin_array;
}
add_filter( 'mce_external_plugins', 'my_add_tinymce_button' );