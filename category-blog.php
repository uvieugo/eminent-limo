<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Eminent_Limo
 */

get_header(); ?>
<div id="blog" class="page blog">
	<div class="container">
		<div class="row">
			<div class="col">
					<div id="content" class="site-content blog">
						<div class="row">
						<div class="card-columns">
						<?php
							if ( have_posts() ) : 
								
								while ( have_posts() ) : the_post(); 
									get_template_part( 'template-parts/content', 'blog' ); 
								endwhile; ?>

						</div>
								<?php
								the_posts_pagination();

								else :
									get_template_part( 'template-parts/content', 'none' );

								endif; 
							?>
							
						</div>

				</div>
			</div>
		</div>
	</div>
</div>
<?php
get_footer();
