<?php
/**
 * The template for displaying all pages
 * Template Name: Other-Page 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Eminent_Limo
 */

get_header(); ?>
<div id="other-pages" class="site">
	<div id="content" class="site-content">
		<header class="entry-header">
			<div class="container">
				<div class="row">
					<div class="col">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</div>
				</div>
			</div>
		</header>
		<div class="container">
			<div class="row">
				<div class="col-md-9">
				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', 'page' );

				endwhile; 
				?>
					
				</div>
				<div class="col-md-3">
					<?php get_sidebar();?>
				</div>
			</div>
		</div>
	</div><!-- #content -->
</div>
<?php

get_footer();
