jQuery('a[href*=#]:not([href=#])').click(function () {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') ||
        location.hostname == this.hostname) {

        var target = jQuery(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            jQuery('html,body').animate({
                scrollTop: target.offset().top
            }, 1000);
            return false;
        }
    }
});

jQuery('#myTab a').on('click', function (e) {
    e.preventDefault()
    jQuery(this).tab('show')
});

jQuery(document).ready(function(){
    jQuery('#carousel-fade').carousel({
        interval: 3000
    });
    // alert('echo');
});