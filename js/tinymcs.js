jQuery(document).on('tinymce-editor-init', function (event, editor) {
    // register the formats
    tinymce.init({
        block_formats: 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3'
    });

    tinymce.activeEditor.formatter.register('test', {
        block: 'div',
        classes: 'post-h3',
    });
    tinymce.activeEditor.formatter.register('pullquote-1', {
        block: 'blockquote',
        classes: 'blockquote-1',
    });
    tinymce.activeEditor.formatter.register('pullquote-2', {
        block: 'blockquote',
        classes: 'blockquote-2',
    });
    tinymce.activeEditor.formatter.register('pullquote-3', {
        block: 'blockquote',
        classes: 'blockquote-3',
    });
});