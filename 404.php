<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Eminent_Limo
 */

get_header(); ?>
<section class="error-404 not-found">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<header class="">
								<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'eminent-limo' ); ?></h1>
							</header>

							<div class="page-content">
								<p class="text-center"><?php esc_html_e( 'Looks like the page you are trying to visit does not exist. Please check the URL and try again.', 'eminent-limo' ); ?></p>
								<p class="text-center"><a href='<?php echo get_home_url(); ?>'>Go to Home Page</a></p>
							</div><!-- .page-content -->
						</div>
					</div>
				</div>
				
			</section><!-- .error-404 -->
<?php
get_footer();
