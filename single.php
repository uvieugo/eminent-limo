<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Eminent_Limo
 */

get_header(); ?>
<div id="page" class="site">
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col col-md-10">
				<div id="content" class="site-content d-flex align-content-stretch flex-wrap">
					<div id="primary" class="content-area">
							<?php
								while ( have_posts() ) : the_post();
			
									get_template_part( 'template-parts/content', get_post_type() );
			
									// the_post_navigation();
			
									// If comments are open or we have at least one comment, load up the comment template.
									// if ( comments_open() || get_comments_number() ) :
									// 	comments_template();
									// endif;
			
								endwhile; // End of the loop.
							?>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<?php
// get_sidebar();
get_footer();
