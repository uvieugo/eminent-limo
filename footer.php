<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Eminent_Limo
 */

?>

	<footer id="colophon" class="site-footer">
		<div class="container">
			<div class="row">
				<div class="box col-sm-3">
					<h3><span class="first-word">About</span> Eminent Limo</h3>
					<p>Eminent Limo is here ready to serve you. AFFORDABLE LUXURY FLEET</p>
				</div>
				<div class="box col-sm-3">
					<h3><span class="first-word">Contact</span> Info</h3>
					<div>
						<p>
							2138 S Indiana Ave <br>
							Chicago, Cook County 60616 <br>
							USA <br>
							<strong>Phone:</strong> 855-286-6691<br>
							<strong>Email:</strong> eminentlimo@gmail.com
						</p>
					</div>
				</div>
				<div class="box col-sm-3">
					<h3><span class="first-word">Service</span> Hours</h3>
					<p><strong>OPEN 24hrs</strong> <br></p>
				</div>
				<div class="box col-sm-3">
					<h3><span class="first-word">Social</span> Network</h3>
					<div class="social">
						<a class="facebook" href="https://www.facebook.com/eminentlimo/" target="_blank"><i class="fa fa-facebook-f "></i></a>
						<a class="twitter" href="https://www.twitter.com/EminentLimo" target="_blank"><i class="fa fa-twitter "></i></a>
						<a class="yelp" href="https://www.yelp.com/biz/eminent-limo-chicago?utm_medium=badge_button&amp;utm_source=biz_review_badge" target="_blank"><i class="fa fa-yelp "></i></a>
						<a class="instagram" href="https://www.instagram.com/eminentlimo/" target="_blank"><i class="fa fa-instagram "></i></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col autorize-badge d-flex justify-content-center">
					<!-- (c) 2005, 2017. Authorize.Net is a registered trademark of CyberSource Corporation --> <div class="AuthorizeNetSeal"> <script type="text/javascript" language="javascript">var ANS_customer_id="be8c1845-e047-428a-988d-105c0ef83383";</script> <script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js" ></script> </div>
				
				</div>
			</div>
			<div class="row">
			<div class="col">
				<div class="site-info">
				All rights reserved © 2017 | Eminent Limo
				</div>
			</div>
			</div>
		</div>
	</footer>


<?php wp_footer(); ?>

</body>
</html>
