<?php
/**
 * The main template file
 * Template Name: Contact Page
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Eminent_Limo
 */

get_header(); ?>

    <div id="contact-page" class="site">
        <div id="content" class="site-content">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <header class="entry-header">
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                                </div>
                            </div>
                        </div>
                    </header>
                    <div class="container">
                        <div class="row">
                            <?php
                                if ( have_posts() ) :

                                    while ( have_posts() ) : the_post();

                                        get_template_part( 'template-parts/content', 'contact' );

                                    endwhile;

                                endif; 
                                
                            ?>
                        </div>
                        <hr>
                    </div>
                </main>
            </div>
        </div>
    </div>
    <?php

get_footer();