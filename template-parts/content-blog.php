<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Eminent_Limo
 */

?>
<div class="card">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( has_post_thumbnail() ) : ?>
			<img class="card-img-top" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="Card image cap">
		<?php endif; ?>
		
		<?php if ( get_post_format() != 'image' ) :?>
		
			<div class="card-body">
				<?php the_title( '<h4 class="card-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>' ); ?>
				<hr>
				<div class="card-text">
					<?php
						the_excerpt( sprintf(
							wp_kses(
								/* translators: %s: Name of current post. Only visible to screen readers */
								__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'eminent-limo' ),
								array(
									'span' => array(
										'class' => array(),
									),
								)
							),
							get_the_title()
						) );

						wp_link_pages( array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'eminent-limo' ),
							'after'  => '</div>',
						) );
					?>
				</div>
			</div>

		<?php endif; ?>
	</article>
	<!-- #post-<?php the_ID(); ?> -->
</div>