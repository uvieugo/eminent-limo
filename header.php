<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Eminent_Limo
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<header class="top fixed-top">
		<div class="top-header text-center text-md-right">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="social">
							<a class="facebook" href="https://www.facebook.com/eminentlimo/" target="_blank"><i class="fa fa-facebook-f "></i></a>
							<a class="twitter" href="https://www.twitter.com/EminentLimo" target="_blank"><i class="fa fa-twitter "></i></a>
							<a class="yelp" href="https://www.yelp.com/biz/eminent-limo-chicago?utm_medium=badge_button&amp;utm_source=biz_review_badge" target="_blank"><i class="fa fa-yelp "></i></a>
							<a class="instagram" href="https://www.instagram.com/eminentlimo/" target="_blank"><i class="fa fa-instagram "></i></a>
						</div>
						<div class="phone">
							<span>Call: <a href="tel:8552866691">855-286-6691</a></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-expand-lg navbar-custom ">
			<div class="container">
				<a class="navbar-brand" href="<?php echo get_site_url(); ?>">
					<img src="<?php echo get_template_directory_uri()."/assets/images/logo.jpg" ?>" alt="" class="img-responsive">
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<?php
					wp_nav_menu([
						'menu'            => 'top',
						'theme_location'  => 'menu-1',
						'container'       => 'div',
						'container_id'    => 'navbarSupportedContent',
						'container_class' => 'collapse navbar-collapse ',
						'menu_id'         => false,
						'menu_class'      => 'navbar-nav ml-auto',
						'depth'           => 2,
						'fallback_cb'     => 'bs4navwalker::fallback',
						'walker'          => new bs4navwalker()
					]);
					?>
			</div>

		</nav>
	</header>

